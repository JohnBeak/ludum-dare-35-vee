﻿using UnityEngine;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour {
    public TextAsset[] inputLevels;

    public GameObject groundPrefab;
    public GameObject wallPrefab;
    public GameObject waterPrefab;

    public GameObject playerPrefab;
    public float playerStepDuration = 0.5f;

    public GameObject targetPrefab;
    public GameObject waterStonePrefab;
    public GameObject thunderstonePrefab;
    public GameObject fireStonePrefab;
    public GameObject cratePrefab;

    public float groundZ = 0;
    public float wallZ = -2;
    public float itemZ = -1;
    public float playerZ = -1.8f;

    public GameObject thunderboltPrefab;
    public float thunderboltDuration = 1f;
    public float thunderboltZ = -3f;
    public GameObject flamePrefab;
    public float flamethrowerDuration = 1.5f;
    public float flamethrowerZ = -1.7f;
    public int flamethrowerFlameCount = 9;

    private int level;

    private GameObject player;
    private bool playerIsMoving;
    private float playerMoveTimeElapsed;
    private int playerCurrentX, playerCurrentY;
    private int playerNextX, playerNextY;
    private Animator playerAnimator;
    private Animator playerShadowAnimator;
    private int playerCurrentCharacter;

    private Object movingCrate;
    private GameObject movingCrateGameObject;
    private bool crateIsMoving;

    private int sizeX, sizeY;
    private Object[,] spawnedFloors;
    private Object[,] spawnedItems;
    private FloorTypes[,] floors;
    private ItemTypes[,] items;
    private GameObject thunderbolt;
    private float thunderboltTimeElapsed;
    private bool thunderboltActive;
    private int flamethrowerTargetX, flamethrowerTargetY;
    private float flamethrowerTimeElapsed;
    private bool flamethrowerActive;
    private GameObject[] flamethrowerFlames;

    enum FloorTypes { Ground, Wall, Water }
    enum ItemTypes { None, Target, Boulder, Crate, WaterStone, ThunderStone, FireStone }


    void Start () {
        level = 0;
        playerIsMoving = false;
        playerMoveTimeElapsed = 0;
        crateIsMoving = false;
        thunderbolt = (GameObject)Instantiate(thunderboltPrefab, new Vector3(0, 0, thunderboltZ), Quaternion.identity);
        thunderbolt.SetActive(false);
        thunderboltTimeElapsed = 0f;
        thunderboltActive = false;
        flamethrowerTimeElapsed = 0f;
        flamethrowerActive = false;
        flamethrowerFlames = new GameObject[flamethrowerFlameCount];
        for (int i = 0; i < flamethrowerFlameCount; i++)
        {
            flamethrowerFlames[i] = (GameObject)Instantiate(flamePrefab, new Vector3(0, 0, flamethrowerZ), Quaternion.identity);
            flamethrowerFlames[i].SetActive(false);
        }

        ReloadMap();
	}

    void Update()
    {
        if (flamethrowerActive) {
            flamethrowerTimeElapsed += Time.deltaTime;
            if (flamethrowerTimeElapsed >= flamethrowerDuration)
            {
                flamethrowerActive = false;
                for (int i = 0; i < flamethrowerFlameCount; i++)
                {
                    flamethrowerFlames[i].SetActive(false);
                }
                ChangeForm(133);

                int dx = 0;
                int dy = 0;

                switch (playerAnimator.GetInteger("Direction"))
                {
                    case 0: // left
                        dx = -1;
                        break;
                    case 1: // right
                        dx = 1;
                        break;
                    case 2: // up
                        dy = -1;
                        break;
                    case 3: // down
                        dy = 1;
                        break;
                }

                for (int i = 1; i < (int)Mathf.Abs(playerCurrentX - flamethrowerTargetX) + (int)Mathf.Abs(playerCurrentY - flamethrowerTargetY); i++)
                {
                    if (items[playerCurrentX + dx * i, playerCurrentY + dy * i] == ItemTypes.Crate)
                    {
                        Destroy(spawnedItems[playerCurrentX + dx * i, playerCurrentY + dy * i]);
                        items[playerCurrentX + dx * i, playerCurrentY + dy * i] = ItemTypes.None;
                    }
                } 
            }

            for (int i = 0; i < flamethrowerFlameCount; i++)
            {
                flamethrowerFlames[i].transform.position = PositionFlame(i, LogicPositionToWorldPosition(playerCurrentX, playerCurrentY, flamethrowerZ), LogicPositionToWorldPosition(flamethrowerTargetX, flamethrowerTargetY, flamethrowerZ), flamethrowerTimeElapsed / flamethrowerDuration);
            }
        }

        if (thunderboltActive) {
            thunderboltTimeElapsed += Time.deltaTime;
            if (thunderboltTimeElapsed >= thunderboltDuration) {
                thunderboltActive = false;
                thunderbolt.SetActive(false);
                ChangeForm(133);

                for (int i = playerCurrentX - 1; i <= playerCurrentX + 1; i++)
                {
                    for (int j = playerCurrentY - 1; j <= playerCurrentY + 1; j++)
                    {
                        if (items[i, j] == ItemTypes.Crate)
                        {
                            Destroy(spawnedItems[i, j]);
                            items[i, j] = ItemTypes.None;
                        }
                    }
                }
            }
        }

        if (playerIsMoving)
        {
            playerMoveTimeElapsed += Time.deltaTime;

            if (crateIsMoving)
            {
                movingCrateGameObject.transform.position = TweenMovement(LogicPositionToWorldPosition(playerNextX, playerNextY, movingCrateGameObject.transform.position.z), LogicPositionToWorldPosition(playerNextX * 2 - playerCurrentX, playerNextY * 2 - playerCurrentY, movingCrateGameObject.transform.position.z), playerMoveTimeElapsed / playerStepDuration);
            };

            if (playerMoveTimeElapsed >= playerStepDuration)
            {
                // animation ended - handle arrival to new tile
                if (crateIsMoving)
                {
                    spawnedItems[playerNextX * 2 - playerCurrentX, playerNextY * 2 - playerCurrentY] = movingCrate;
                    spawnedItems[playerNextX, playerNextY] = null;
                    movingCrate = null;
                    movingCrateGameObject = null;
                    crateIsMoving = false;
                    items[playerNextX * 2 - playerCurrentX, playerNextY * 2 - playerCurrentY] = items[playerNextX, playerNextY];
                    items[playerNextX, playerNextY] = ItemTypes.None;
                }

                if (floors[playerNextX, playerNextY] == FloorTypes.Water)
                {
                    playerShadowAnimator.SetInteger("Water", 1);
                }
                else if(floors[playerCurrentX, playerCurrentY] == FloorTypes.Water) {
                    playerShadowAnimator.SetInteger("Water", 0);
                    ChangeForm(133);
                }

                playerIsMoving = false;

                playerCurrentX = playerNextX;
                playerCurrentY = playerNextY;

                if (items[playerCurrentX, playerCurrentY] != ItemTypes.None) {
                    PickUpItem(playerCurrentX, playerCurrentY);
                }

            }
            player.transform.position = TweenMovement(LogicPositionToWorldPosition(playerCurrentX, playerCurrentY, playerZ), LogicPositionToWorldPosition(playerNextX, playerNextY, playerZ), playerMoveTimeElapsed / playerStepDuration);
        }
        else if(!thunderboltActive && !flamethrowerActive) {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                playerAnimator.SetInteger("Direction", 2);
                MoveTo(playerCurrentX, playerCurrentY - 1);
            }
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                playerAnimator.SetInteger("Direction", 3);
                MoveTo(playerCurrentX, playerCurrentY + 1);
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                playerAnimator.SetInteger("Direction", 0);
                MoveTo(playerCurrentX - 1, playerCurrentY);
            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                playerAnimator.SetInteger("Direction", 1);
                MoveTo(playerCurrentX + 1, playerCurrentY);
            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (!(playerCurrentCharacter == 134 && floors[playerCurrentX, playerCurrentY] == FloorTypes.Water))
                {
                    // execute move
                    if (playerCurrentCharacter == 135)
                    {
                        AttackThunderbolt(playerCurrentX, playerCurrentY);
                    }
                    if (playerCurrentCharacter == 136) {
                        AttackFlamethrower();
                    }
                }
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                ClearMap();
                System.Threading.Thread.Sleep(1000);
                ReloadMap();
            }
        }
    }

    void AttackThunderbolt(int x, int y) {
        thunderbolt.transform.position = LogicPositionToWorldPosition(x, y, thunderbolt.transform.position.z);
        thunderbolt.SetActive(true);
        thunderboltTimeElapsed = 0f;
        thunderboltActive = true;
    }

    void AttackFlamethrower() {
        int dx = 0;
        int dy = 0;

        switch (playerAnimator.GetInteger("Direction")) {
            case 0: // left
                dx = -1;
                break;
            case 1: // right
                dx = 1;
                break;
            case 2: // up
                dy = -1;
                break;
            case 3: // down
                dy = 1;
                break;
        }

        int availableDistance = 0;

        for (int i = 0; i < 4; i++)
        {
            if (floors[playerCurrentX + dx * i, playerCurrentY + dy * i] == FloorTypes.Wall)
            {
                break;
            }
            availableDistance++;
        }

        flamethrowerTargetX = playerCurrentX + dx * availableDistance;
        flamethrowerTargetY = playerCurrentY + dy * availableDistance;

        flamethrowerActive = true;
        flamethrowerTimeElapsed = 0;
        for (int i = 0; i < flamethrowerFlameCount; i++)
        {
            flamethrowerFlames[i].transform.position = LogicPositionToWorldPosition(playerCurrentX, playerCurrentY, flamethrowerZ);
            flamethrowerFlames[i].SetActive(true);
        }
    }

    Vector3 PositionFlame(int order, Vector3 startPoint, Vector3 endPoint, float t) {
        Vector3 truncatedEndPoint = new Vector3(
            endPoint.x - (Mathf.Abs(endPoint.x - startPoint.x) > 0.5f ? Mathf.Sign(endPoint.x - startPoint.x) * 0.75f : 0f),
            endPoint.y - (Mathf.Abs(endPoint.y - startPoint.y) > 0.5f ? Mathf.Sign(endPoint.y - startPoint.y) * 0.75f : 0f),
            endPoint.z
            );

        float amplitude = 0.1f;

        float dx = (playerAnimator.GetInteger("Direction") < 2 ? 0f : 1f);
        float dy = 1f - dx;

        float distanceFromPlayer = (float)(order + 1) / (float)(flamethrowerFlameCount);

        float distanceFromLine = Mathf.Sin(Mathf.PI * 2f * distanceFromPlayer);

        float phase = Mathf.Sin(Mathf.PI * 4f * t);

        return new Vector3(
            startPoint.x * (1f - distanceFromPlayer) + truncatedEndPoint.x * distanceFromPlayer + amplitude * dx * distanceFromLine * phase,
            startPoint.y * (1f - distanceFromPlayer) + truncatedEndPoint.y * distanceFromPlayer + amplitude * dy * distanceFromLine * phase,
            startPoint.z
            );
    }

    void ChangeForm(int playerNewCharacter) {
        playerCurrentCharacter = playerNewCharacter;
        playerAnimator.SetInteger("Character", playerCurrentCharacter);
    }

    void PickUpItem(int x, int y) {
        switch (items[x, y]) {
            case ItemTypes.Target:
                Debug.Log("A winner is you!");
                ClearMap();
                System.Threading.Thread.Sleep(1000);
                level++;
                ReloadMap();
                return;
            case ItemTypes.WaterStone:
                if (playerCurrentCharacter == 133)
                {
                    ChangeForm(134);
                }
                else {
                    return;
                }
                break;
            case ItemTypes.ThunderStone:
                if (playerCurrentCharacter == 133)
                {
                    ChangeForm(135);
                }
                else {
                    return;
                }
                break;
            case ItemTypes.FireStone:
                if (playerCurrentCharacter == 133)
                {
                    ChangeForm(136);
                }
                else {
                    return;
                }
                break;
            case ItemTypes.Crate:
                return;
        }

        Destroy(spawnedItems[x, y]);
        items[x, y] = ItemTypes.None;
    }

    Vector3 TweenMovement(Vector3 startVector, Vector3 endVector, float t)
    {
        float tweenedT = Mathf.Sin(t * Mathf.PI / 2f);
        return new Vector3((float)startVector.x * (1f - tweenedT) + (float)endVector.x * tweenedT, (float)startVector.y * (1f - tweenedT) + (float)endVector.y * tweenedT, startVector.z);
    }

    void MoveTo(int targetX, int targetY)
    {
        if (items[targetX, targetY] != ItemTypes.None)
        {
            if (items[targetX, targetY] == ItemTypes.Crate) {
                if (MoveCrateTo(targetX, targetY, 2 * targetX - playerCurrentX, 2 * targetY - playerCurrentY))
                {

                }
                else {
                    return;
                }
            }
        }

        if(playerCurrentCharacter == 134 && floors[targetX, targetY] == FloorTypes.Water)
        {

        }
        else if (floors[targetX, targetY] != FloorTypes.Ground)
        {
            return;
        }

        playerIsMoving = true;
        playerMoveTimeElapsed = 0f;
        playerNextX = targetX;
        playerNextY = targetY;
    }

    bool MoveCrateTo(int fromX, int fromY, int toX, int toY) {
        if (floors[toX, toY] != FloorTypes.Ground) { return false; }
        if (items[toX, toY] != ItemTypes.None) { return false; }

        crateIsMoving = true;
        movingCrate = spawnedItems[fromX, fromY];
        movingCrateGameObject = (GameObject)movingCrate;

        return true;
    }

    void ReloadMap() {
        if (inputLevels[level] == null) { return; }

        try {

            string[] lines = inputLevels[level].text.Split(new string[] { System.Environment.NewLine }, System.StringSplitOptions.RemoveEmptyEntries);
            sizeX = lines.Length;
            sizeY = lines[0].Length;

            floors = new FloorTypes[sizeX, sizeY];
            items = new ItemTypes[sizeX, sizeY];
            spawnedFloors = new Object[sizeX, sizeY];
            spawnedItems = new Object[sizeX, sizeY];

            int x = 0, y = 0;

            foreach (string line in lines) {
                foreach (char character in line) {
                    floors[x, y] = FloorTypes.Ground;
                    items[x, y] = ItemTypes.None;
                    switch (character)
                    {
                        case ' ':
                            spawnedFloors[x, y] = Instantiate(groundPrefab, LogicPositionToWorldPosition(x, y, groundZ), Quaternion.identity);
                            floors[x, y] = FloorTypes.Ground;
                            break;
                        case '#':
                            spawnedFloors[x, y] = Instantiate(wallPrefab, LogicPositionToWorldPosition(x, y, wallZ - (float)(x+y)*0.02f), Quaternion.identity);
                            floors[x, y] = FloorTypes.Wall;
                            break;
                        case 'W':
                            spawnedFloors[x, y] = Instantiate(waterPrefab, LogicPositionToWorldPosition(x, y, groundZ), Quaternion.identity);
                            floors[x, y] = FloorTypes.Water;
                            break;
                        case 'S':
                            spawnedFloors[x, y] = Instantiate(groundPrefab, LogicPositionToWorldPosition(x, y, groundZ), Quaternion.identity);
                            floors[x, y] = FloorTypes.Ground;
                            player = (GameObject)Instantiate(playerPrefab, LogicPositionToWorldPosition(x, y, playerZ), Quaternion.identity);
                            playerCurrentX = x;
                            playerCurrentY = y;
                            playerNextX = x;
                            playerNextY = y;
                            playerAnimator = player.transform.Find("Player").GetComponent<Animator>();
                            playerAnimator.SetTrigger("Direction");
                            playerAnimator.SetTrigger("Character");
                            playerShadowAnimator = player.transform.Find("Shadow").GetComponent<Animator>();
                            playerCurrentCharacter = 133;
                            break;
                        case 'X':
                            spawnedFloors[x, y] = Instantiate(groundPrefab, LogicPositionToWorldPosition(x, y, groundZ), Quaternion.identity);
                            floors[x, y] = FloorTypes.Ground;
                            spawnedItems[x, y] = Instantiate(targetPrefab, LogicPositionToWorldPosition(x, y, itemZ), Quaternion.identity);
                            items[x, y] = ItemTypes.Target;
                            break;
                        case '1':
                            spawnedFloors[x, y] = Instantiate(groundPrefab, LogicPositionToWorldPosition(x, y, groundZ), Quaternion.identity);
                            floors[x, y] = FloorTypes.Ground;
                            spawnedItems[x, y] = Instantiate(waterStonePrefab, LogicPositionToWorldPosition(x, y, itemZ), Quaternion.identity);
                            items[x, y] = ItemTypes.WaterStone;
                            break;
                        case '2':
                            spawnedFloors[x, y] = Instantiate(groundPrefab, LogicPositionToWorldPosition(x, y, groundZ), Quaternion.identity);
                            floors[x, y] = FloorTypes.Ground;
                            spawnedItems[x, y] = Instantiate(thunderstonePrefab, LogicPositionToWorldPosition(x, y, itemZ), Quaternion.identity);
                            items[x, y] = ItemTypes.ThunderStone;
                            break;
                        case '3':
                            spawnedFloors[x, y] = Instantiate(groundPrefab, LogicPositionToWorldPosition(x, y, groundZ), Quaternion.identity);
                            floors[x, y] = FloorTypes.Ground;
                            spawnedItems[x, y] = Instantiate(fireStonePrefab, LogicPositionToWorldPosition(x, y, itemZ), Quaternion.identity);
                            items[x, y] = ItemTypes.FireStone;
                            break;
                        case 'C':
                            spawnedFloors[x, y] = Instantiate(groundPrefab, LogicPositionToWorldPosition(x, y, groundZ), Quaternion.identity);
                            floors[x, y] = FloorTypes.Ground;
                            spawnedItems[x, y] = Instantiate(cratePrefab, LogicPositionToWorldPosition(x, y, itemZ), Quaternion.identity);
                            items[x, y] = ItemTypes.Crate;
                            break;
                    }
                    x++;
                }
                y++;
                x = 0;
            }
        }
        catch{ }
    }

    Vector3 LogicPositionToWorldPosition(int logicX, int logicY, float currentZ)
    {
        return new Vector3(-(float)sizeX / 2f + (float)logicX, (float)sizeY / 2f - (float)logicY, currentZ);
    }

    void ClearMap() {
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                if (spawnedFloors[x, y] != null) {
                    Destroy(spawnedFloors[x, y]);
                }
                if (spawnedItems[x, y] != null)
                {
                    Destroy(spawnedItems[x, y]);
                }
            }
        }
        if (player != null) {
            Destroy(player);
        }
    }
}
