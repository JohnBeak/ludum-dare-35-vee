﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour {
    public string nextScene = "Menu";

    void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(nextScene);
        }
	}
}
