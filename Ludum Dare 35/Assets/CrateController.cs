﻿using UnityEngine;
using System.Collections;

public class CrateController : MonoBehaviour {
    public float animationStart = 0.0f;
    public float fallDuration = 1.0f;
    public float shakeBegin = 2.0f;
    public float explode = 3.0f;
    public float changeScene = 5.0f;

    public string nextScene;

    private float timeElapsed;

	// Use this for initialization
	void Start () {
        timeElapsed = 0;
	}
	
	// Update is called once per frame
	void Update () {
        timeElapsed += Time.deltaTime;
        if (timeElapsed > animationStart && timeElapsed < fallDuration)
        {
            float t = (timeElapsed - animationStart) / (fallDuration - animationStart);
            transform.position = new Vector3(0, Mathf.Sin((0.2f + 0.8f * t) * Mathf.PI) * Mathf.Pow(1f - t, 2) * 20f, 0);
        }
        else if (timeElapsed > fallDuration && timeElapsed < shakeBegin)
        {
            transform.position = Vector3.zero;
        }
        else if (timeElapsed > shakeBegin && timeElapsed < explode)
        {
            float t = (timeElapsed - shakeBegin) / (explode - shakeBegin);
            float amplitude = Mathf.Max(0f, Mathf.Min(t * 0.05f, 1.0f));
            float crazySin = Mathf.Round(t * 360f);
            transform.position = new Vector3(Mathf.Sin(crazySin) * amplitude, Mathf.Cos(crazySin) * amplitude, 0);
        }
        else if (timeElapsed > explode && timeElapsed < changeScene) {
            transform.position = Vector3.zero;
            Color oldColor = GetComponent<SpriteRenderer>().color;
            GetComponent<SpriteRenderer>().color = new Color(oldColor.r, oldColor.g, oldColor.b, 0);
        }
        else if (timeElapsed > changeScene)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(nextScene);
        }
	}
}
